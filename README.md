## React

https://github.com/enaqx/awesome-react - community

### DOC

https://reactjs.org/
- https://en.reactjs.org/docs/error-boundaries.html
- https://reactjs.org/blog/2022/03/29/react-v18.html

https://overreacted.io/ - blog twórcy reacta 

https://kentcdodds.com/blog 
- https://epicreact.dev/articles


### UI 

https://mui.com/getting-started/usage/ - mui 10/10

### Routing 

https://www.robinwieruch.de/react-router/ - tutorial

### SVG

https://react-svgr.com/ - konwersja z svg na komponent reactowy

### Komunikacja 

https://react-query.tanstack.com/overview
- https://react-query.tanstack.com/guides/infinite-queries
- https://react-query.tanstack.com/reference/QueryClient
- https://react-query.tanstack.com/reference/useMutation


## React Native

https://github.com/jondot/awesome-react-native - community

### DOC

https://reactnative.dev/docs/getting-started
- https://reactnative.dev/docs/animations - animacje 
- https://reactnative.dev/blog/2020/07/06/version-0.63#pressable - info dlaczego Pressable wypiera TouchableOpacity
- https://reactnative.dev/docs/flexbox#flex - flexbox w RN

### Navigation 

https://reactnavigation.org/ - nawigacja 
- https://reactnavigation.org/docs/typescript/
- https://reactnavigation.org/docs/params/#what-should-be-in-params
- https://reactnavigation.org/docs/nesting-navigators/#passing-params-to-a-screen-in-a-nested-navigator


### UI

https://www.npmjs.com/package/react-native-skeleton-placeholder - ładne placeholdery 

### Utils

https://github.com/zoontek/react-native-permissions - uprawnienia

https://github.com/th3rdwave/react-native-safe-area-context - save area ios/andro

https://github.com/hampusborgos/country-flags - flagi różnych krajów

https://github.com/react-native-async-storage/async-storage - cos ala local storage 

https://github.com/RonRadtke/react-native-blob-util - do przesyłania duzych plików

https://github.com/react-native-device-info/react-native-device-info - info o buildie/systemie/parametrach telefonu

https://github.com/APSL/react-native-keyboard-aware-scroll-view - obsługa klawiatury w ekranach ze scrollem 


## TypeScript

https://www.typescriptlang.org/docs
- https://www.typescriptlang.org/docs/handbook/enums.html - enumy
- https://www.typescriptlang.org/docs/handbook/utility-types.html#recordkeys-type - rekordy


## JS

### DOC

https://javascript.info/ - praktyczny tutorial

- https://www.digitalocean.com/community/tutorials/js-es2019 - co nowego w ES 10
- https://programmingwithmosh.com/javascript/whats-new-in-ecmascript-2020/ - co nowego w ES 11
- https://www.geeksforgeeks.org/new-features-in-ecmascript-2021-update/ - co nowego w ES 12

### Playground 

https://www.codewars.com/ - mozna wbijac poziomy jak w tekkenie :D

### 3D 

https://threejs.org/ - paczka do generowania elementow 3D
- https://www.youtube.com/watch?v=Q7AOvWpIVHU - tutorial

### Utils

- https://developer.mozilla.org/pl/docs/Web/JavaScript/Reference/Global_Objects/encodeURI - hack na przesyłanie złożonych stringow w parameters 

## Next.js

### DOC

https://nextjs.org/docs/getting-started
- https://www.youtube.com/watch?v=MFuwkrseXVE - tutorial od niemca z udemy (podstawy podstaw)


## GIT 

https://commitizen-tools.github.io/commitizen/ - tool do standaryzacji commitow

### CI/CD

https://github.com/renovatebot/renovate - automat do informowania o problemach z paczkami ( ala dependobot z github-a)

## IOS

### terminal 

https://github.com/ohmyzsh/ohmyzsh - framework do termiala 

### Xcode

https://medium.com/flawless-app-stories/change-splash-screen-in-ios-app-for-dummies-the-better-way-e385327219e - dodawanie splash screena

http://onebigfunction.com/ios/2017/03/14/flavor-ios/ - flavoury i ikony 
- *Keep in mind that app icon and splash screen should be changed manually in general -> App icons and launch images* 

## Android

### Utils
- https://developer.android.com/studio/command-line/adb - dev mode na fizyczne urządzenie

- https://github.com/roadmanfong/luxon/blob/master/docs/install.md#react-native - support do intl-a

## CSS

- https://neumorphism.io/ - generator do cieni

## Umiejętności miękkie 

https://www.youtube.com/watch?v=LNvatoTUCzE - how to CR

## Wiedza ogólna 

- https://pl.wikipedia.org/wiki/Zrobotyzowana_Automatyzacja_Proces%C3%B3w

### Przydatne linki

- https://strumentidev.it/partita-iva - generator do włoskiego vatID
- http://generatory.it/ - generatory nipu/ibanu/peselu
- https://www.ovhcloud.com/ - mozna kupic domene
- https://www.cloudflare.com/ - podobno mozna zamaskowac częściowo swój ruch + zabezpieczyc sie przed ddosami
